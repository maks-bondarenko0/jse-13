package ru.t1.bondarenko.tm.controller;

import ru.t1.bondarenko.tm.api.controller.IProjectController;
import ru.t1.bondarenko.tm.api.service.IProjectService;
import ru.t1.bondarenko.tm.api.service.IProjectTaskService;
import ru.t1.bondarenko.tm.enumerated.Sort;
import ru.t1.bondarenko.tm.enumerated.Status;
import ru.t1.bondarenko.tm.model.Project;
import ru.t1.bondarenko.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    private final IProjectTaskService projectTaskService;

    public ProjectController(final IProjectService projectService, final IProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void showProjects() {
        System.out.println("[SHOW PROJECTS]");
        System.out.println("[ENTER SORT]");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Project> projects = projectService.findAll(sort);
        showProjectList(projects);
        System.out.println("[OK]");
    }

    public void showProjectList(final List<Project> projects) {
        int index = 1;
        for (final Project project : projects) {
            System.out.printf("%s[%s]. %s \n", index, project.getId(), project);
            index++;
        }
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
    }

    @Override
    public void clearProjects() {
        System.out.println("[PROJECT CLEAR]");
        projectTaskService.removeProjects();
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectByID() {
        System.out.println("[DELETE PROJECT BY ID]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        projectService.removeByID(id);
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[DELETE PROJECT BY INDEX]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        projectService.removeByIndex(index);
    }

    private void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
    }

    @Override
    public void showProjectByID() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findByID(id);
        showProject(project);
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findByIndex(index);
        showProject(project);
    }

    @Override
    public void updateProjectByID() {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        projectService.updateByID(id, name, description);
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        projectService.updateByIndex(index, name, description);
    }

    @Override
    public void startProjectById() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        projectService.changeProjectStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    public void startProjectByIndex() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        projectService.changeProjectStatusByIndex(index, Status.IN_PROGRESS);
    }

    @Override
    public void completeProjectById() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        projectService.changeProjectStatusById(id, Status.COMPLETED);
    }

    @Override
    public void completeProjectByIndex() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        projectService.changeProjectStatusByIndex(index, Status.COMPLETED);
    }

    @Override
    public void changeProjectStatusById() {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        projectService.changeProjectStatusById(id, status);
    }

    @Override
    public void changeProjectStatusByIndex() {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        projectService.changeProjectStatusByIndex(index, status);
    }

}
