package ru.t1.bondarenko.tm.exception.system;

public class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {
        super("Error! Argument not supported...");
    }

    public ArgumentNotSupportedException(final String message) {
        super("Error! Argument \"" + message + "\" not supported...");
    }

}
