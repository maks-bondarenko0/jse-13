package ru.t1.bondarenko.tm.api.repository;

import ru.t1.bondarenko.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
