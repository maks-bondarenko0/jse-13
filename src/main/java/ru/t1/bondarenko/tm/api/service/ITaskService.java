package ru.t1.bondarenko.tm.api.service;

import ru.t1.bondarenko.tm.api.repository.ITaskRepository;
import ru.t1.bondarenko.tm.enumerated.Sort;
import ru.t1.bondarenko.tm.enumerated.Status;
import ru.t1.bondarenko.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends ITaskRepository {

    Task create(String name);

    Task create(String name, String description);

    Task updateByID(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    List<Task> findAll(Sort sort);

}
